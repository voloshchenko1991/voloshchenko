package com.dvo;

public class Task1 {
    public static final String farmer[] = {
            "   ,,,,",
            "  /   '",
            " /.. /",
            "( c  D",
            " \\- '\\_",
            "  `-'\\)\\",
            "     |_ \\",
            "     |U \\\\",
            "    (__,//",
            "    |. \\/",
            "    LL__I",
            "     |||",
            "     |||",
            "  ,,-``'\\"
    };
    public static final String boat[] = {
            "<--._______:U~~~~~~~~\\_________.:---/",
            " \\      ____===================____/",
            ",-~\"^\"~-,.,-~\"^\"~-,.,-~\"^\"~-,.,-~\"^\"~-,.",
            "^\"~-,.,-~\"^\"~-,.,-~\"^\"~-,.,-~\"^\"~-,.,-~\"",
            "~-,.,-~\"^\"~-,.,-~\"^\"~-,.,-~\"^\"~-,.,-~\"^\""
    };
    public static final String cabbage[] = {
            "  .-~~~~-.",
            " /  ( ( ' \\",
            "| ( )   )  |",
            "\\ ) ' }  / /",
            "(` \\ , /  ~)",
            " `-.`\\/_.-'",
            "    `\"\""
    };
    public static final String goat[] = {
            "    _))",
            "   > *\\     _~",
            "   `;'\\\\__-' \\_",
            "      | )  _ \\ \\",
            "     / / ``   w w",
            "    w w"
    };
    public static final String wolf[] = {
            " )      (\\_",
            "((    _/{  \"-;",
            " )).-' {{ ;'`",
            "( (  ;._ \\\\"
    };

    public static void main(String[] args) {
        drawLine();
        String[] empty = {};
        String[] imgBoat = drawBoat(true, cabbage);
        String[] temp1 = drawHorizontalPair(goat, wolf);
        String[] temp2 = drawHorizontalPair(farmer,cabbage);
        String[] leftCoast = drawVerticalPair(temp1,temp2);
        String[] rightCoast = {};
        draw(imgBoat, leftCoast, rightCoast);
        drawLine();

        imgBoat = drawBoat(false,goat);
        leftCoast = drawHorizontalPair(wolf,cabbage);
        draw(imgBoat,leftCoast,rightCoast);
        drawLine();

        imgBoat = drawBoat(false,empty);
        leftCoast = drawHorizontalPair(wolf,cabbage);
        rightCoast = goat;
        draw(imgBoat,leftCoast,rightCoast);
        drawLine();

        imgBoat = drawBoat(false,cabbage);
        leftCoast = wolf;
        rightCoast = goat;
        draw(imgBoat,leftCoast,rightCoast);
        drawLine();

        imgBoat = drawBoat(false,goat);
        leftCoast = wolf;
        rightCoast = cabbage;
        draw(imgBoat,leftCoast,rightCoast);
        drawLine();

        imgBoat = drawBoat(false,wolf);
        leftCoast = goat;
        rightCoast = cabbage;
        draw(imgBoat,leftCoast,rightCoast);
        drawLine();

        imgBoat = drawBoat(false,empty);
        leftCoast = goat;
        rightCoast = drawHorizontalPair(cabbage, wolf);
        draw(imgBoat,leftCoast,rightCoast);
        drawLine();

        imgBoat = drawBoat(false,goat);
        leftCoast = empty;
        rightCoast = drawHorizontalPair(cabbage, wolf);
        draw(imgBoat,leftCoast,rightCoast);
        drawLine();

        imgBoat = drawBoat(true, cabbage);
        temp1 = drawHorizontalPair(goat, wolf);
        temp2 = drawHorizontalPair(farmer,cabbage);
        rightCoast = drawVerticalPair(temp1,temp2);
        leftCoast = empty;
        draw(imgBoat, leftCoast, rightCoast);
        drawLine();



    }

    public static void drawLine(){
        System.out.println();
        for (int i = 0; i < 160; i++) {
            System.out.print("*");
        }
        System.out.println();
    }

    public static void draw(String[] finalBoat, String[] leftCoastItems, String[] rightCoastItems) {                //Main draw method, which is generating
        String result[] = {};
        int height = 0;
        int widthLineRC = 0, widthLineFB = 0, widthLineLC = 0;
        int width = checkWidth(finalBoat) + checkWidth(leftCoastItems) + checkWidth(rightCoastItems);
        if (finalBoat.length > height) height = finalBoat.length;
        if (leftCoastItems.length > height) height = leftCoastItems.length;
        if (rightCoastItems.length > height) height = rightCoastItems.length;
        result = new String[height];
        for (int i = 0; i < height; i++) {
            result[i] = "";
        }
        for (int i = 0; i < height; i++) {
            if (i < finalBoat.length) {
                widthLineFB = finalBoat[i].length();
            } else {
                widthLineFB = 0;
            }
            if (i < leftCoastItems.length) {
                widthLineLC = leftCoastItems[i].length();
            } else {
                widthLineLC = 0;
            }
            if (i < rightCoastItems.length) {
                widthLineRC = rightCoastItems[i].length();
            } else {
                widthLineRC = 0;
            }

            if (widthLineLC != 0) {
                result[i] += leftCoastItems[i];
            }

            if (widthLineFB != 0) {
                result[i] += addMargin(45 - widthLineLC);
                result[i] += finalBoat[i];
            }

            if (widthLineRC != 0) {
                result[i] += addMargin(100 - result[i].length());
                result[i] += rightCoastItems[i];
            }
        }

        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }
    }


    public static String[] drawVerticalPair(String[] topItem, String[] bottomItem) {
        String[] result = {};
        int height = topItem.length + bottomItem.length + 1;
        result = new String[height];
        for (int i = 0; i < height; i++) {
            result[i] = "\n";
        }
        for (int i = 0; i < topItem.length; i++) {
            result[i] = topItem[i];
        }
        for (int i = topItem.length + 1; i < result.length; i++) {
            result[i] = bottomItem[i - (topItem.length + 1)];
        }
        return (result);
    }

    public static String[] drawHorizontalPair(String[] firstItem, String[] secondItem) {
        String[] result = {};
        int height = 0;
        int firstWidth = checkWidth(firstItem);
        int secondWidth = checkWidth(secondItem);
        int width = firstWidth + secondWidth;
        if (firstItem.length > secondItem.length) {
            height = firstItem.length;
        } else {
            height = secondItem.length;
        }
        result = new String[height];
        for (int i = 0; i < result.length; i++) {
            result[i] = "";
        }
        for (int i = 0; i < height; i++) {
            if (i < firstItem.length) {
                result[i] += firstItem[i];
            }
            if ((i < firstItem.length) && (i < secondItem.length)) {
                result[i] += addMargin(width - firstWidth - firstItem[i].length()+10);
            }
            if ((i >= firstItem.length) && (i < secondItem.length)) {
                result[i] += addMargin( width - firstWidth + 10 );
            }
            if (i < secondItem.length) {
                result[i] += secondItem[i];
            }
        }
        return (result);
    }

    public static String[] drawBoat(boolean isEmpty, String[] inBoatItem) {
        String[] result = {};
        String[] cutFarmer = {};
        String[] cutItem = {};
        int boatWidth = 0;
        int itemWidth = 0;
        int farmerWidth = 0;
        int margins = 0;
        int totalHeight = boat.length;
        int startFarmer = 0, startItem = 0, startBoat = 0;
        if (isEmpty) {
            result = boat;
        } else {
            cutFarmer = cutImage(50, farmer);
            cutItem = cutImage(75, inBoatItem);
            boatWidth = checkWidth(boat);
            itemWidth = checkWidth(cutItem);
            farmerWidth = checkWidth(cutFarmer);
            margins = checkMargins(boatWidth, farmerWidth, itemWidth);
            if (cutFarmer.length > cutItem.length) {
                totalHeight += cutFarmer.length;
                startFarmer = 0;
                startBoat = cutFarmer.length;
                startItem = cutFarmer.length - cutItem.length;
            } else {
                totalHeight += cutItem.length;
                startItem = 0;
                startBoat = cutItem.length;
                startFarmer = cutItem.length - cutFarmer.length;
            }
            result = new String[totalHeight];
            for (int i = 0; i < totalHeight; i++) {
                result[i] = "";
            }
            for (int i = 0; i < totalHeight; i++) {
                if (i < startBoat) {
                    if (i >= startFarmer) {
                        result[i] += addMargin(margins);
                        result[i] += cutFarmer[i - startFarmer];
                    }
                    if (i >= startItem) {
                        result[i] += addMargin(boatWidth - result[i].length() - margins*4);
                        result[i] += cutItem[i - startItem];
                    }
                } else {
                    result[i] = boat[i - startBoat];
                }
            }

        }
        return (result);
    }

    public static String addMargin(int number) {
        String result = "";
        for (int i = 0; i < number; i++) {
            result += " ";
        }
        return (result);
    }

    public static String[] cutImage(int percentOfCut, String[] inputImage) {
        String[] result;
        int inputPercent = percentOfCut;
        int imageHeight = inputImage.length;
        int resultHeight = 0;
        if (inputPercent < 20) {
            inputPercent = 20;
        }                                                                                                               //Check for illegal percentage.
        if (inputPercent > 80) {
            inputPercent = 80;
        }
        resultHeight = (imageHeight * inputPercent) / 100;
        result = new String[resultHeight];
        for (int i = 0; i < resultHeight; i++) {
            result[i] = inputImage[i];
        }
        return (result);
    }

    public static int checkWidth(String[] inputData) {                                                                  //Retrieving maximum width of the ASCII picture.
        int result = 0;                                                                                                 //Using in calculations of the margins.
        for (int i = 0; i < inputData.length; i++) {
            if (inputData[i].length() > result) result = inputData[i].length();
        }
        return (result);
    }

    public static int checkMargins(int baseWidth, int leftItem, int rightItem) {                                    //Retrieving similar margins from borders
        int result = 0;                                                                                                //of the boat to both items.
        result = (baseWidth - leftItem - rightItem) / 3;                                                        //Also calculate the same distance from
        return result;                                                                                                  //the first item to the second one.
    }
}
